ruta_local=/work1/gsioper/cargas/scripts/reportesUSSD
archivo_log=bitacora_proc_menu.log
archivo_o=ussd_123_op.log
archivo_ex=ussd_123.log
odate=$1
error_script=0

cd $ruta_local 

echo "Inicio del Proceso " `date +%Y/%m/%d-%H:%M:%S` >> $archivo_log
anio=`echo $odate | awk '{print substr($1, 1, 4)}'`
mes=`echo $odate | awk '{print substr($1, 5, 2)}'`
dia=`echo $odate | awk '{print substr($1, 7)}'`
archivo_or=$archivo_o.$anio"-"$mes"-"$dia

rm -vf $archivo_ex.$anio"-"$mes"-"$dia
cant_arch=`find . -name $archivo_or | wc -l`
if [ cant_arch != "0" ]
then
   for archivo in `find . -name $archivo_or`
   do
   echo "Se proceso el archivo -> $archivo " >> $archivo_log
   # 9298 Modificacion de la secuencia de los log GLIMONES 
	awk  -F\| ' 
	{ num=1; PPAOK[9]=9  ; PPANOK[236]; RPPAOK[236]; TARNOK[236]; RTAROK[236]; AUTNOK[236]; RAUTOK[236]; 
			 fecha2=substr ($0,1,11);
	  # Producto Prepago
	  if( $3=="PPA" && $5=="OK"  ) {
		  
	       for ( j = 1 ; j <= 236 ; j++) { 
		  if ( $4 == j )  {
		    RPPAOK[j]=RPPAOK[j]+1;  
		  
		  } 
	      }
	   }

	    if( $3=="PPA" && $5=="NOK"  ) {
		 for ( i = 1 ; i <= 236 ; i++) { 
		  if ( $4 == i )  {
		     PPANOK[i]=PPANOK[i]+1;  
		  } 
	      }
	    }

	# Producto TAR
	  if( $3=="TAR" && $5=="OK"  ) {
		  
	       for ( j = 1 ; j <= 236 ; j++) { 
		  if ( $4 == j )  {
		    RTAROK[j]=RTAROK[j]+1;  
		  
		  } 
	      }
	   }

	    if( $3=="TAR" && $5=="NOK"  ) {
		 for ( i = 1 ; i <= 236 ; i++) { 
		  if ( $4 == i )  {
		     TARNOK[i]=TARNOK[i]+1;  
		  } 
	      }
	    }
	  

	# Producto AUT
	  if( $3=="AUT" && $5=="OK"  ) {
		  
	       for ( j = 1 ; j <= 236 ; j++) { 
		  if ( $4 == j )  {
		    RAUTOK[j]=RAUTOK[j]+1;  
		  
		  } 
	      }
	   }

	    if( $3=="AUT" && $5=="NOK"  ) {
		 for ( i = 1 ; i <= 236 ; i++) { 
		  if ( $4 == i )  {
		     AUTNOK[i]=AUTNOK[i]+1;  
		  } 
	      }
	    }

	   num++; 

	 }END {
	     # Producto Prepago    
	     for ( i in RPPAOK )  {
	       
		 if ( RPPAOK[i] != null ) {
		printf("%s|%s|%s|%s|%s\n",fecha2,"PPA",i,RPPAOK[i],"OK");
		}

	     }
	    
	     for ( j in PPANOK )  {
		if ( PPANOK[j] != null ) {  
		printf("%s|%s|%s|%s|%s\n",fecha2,"PPA",j,PPANOK[j],"NOK");
		  }
	     }

	     # Producto Tarifario
	     for ( i in RTAROK )  {
	       
		 if ( RTAROK[i] != null ) {
		printf("%s|%s|%s|%s|%s\n",fecha2,"TAR",i,RTAROK[i],"OK");
		}

	     }
	    
	     for ( j in TARNOK )  {
		if ( TARNOK[j] != null ) {  
		printf("%s|%s|%s|%s|%s\n",fecha2,"TAR",j,TARNOK[j],"NOK");
		  }
	     }

	     # Producto Autocontrol
	     for ( i in RAUTOK )  {
	       
		 if ( RAUTOK[i] != null ) {
		printf("%s|%s|%s|%s|%s\n",fecha2,"AUT",i,RAUTOK[i],"OK");
		}

	     }
	    
	     for ( j in AUTNOK )  {
		if ( AUTNOK[j] != null ) {  
		printf("%s|%s|%s|%s|%s\n",fecha2,"AUT",j,AUTNOK[j],"NOK");
		  }
	     }



	 } '  $archivo >> $archivo_ex"."$anio"-"$mes"-"$dia

	error_script=0
   done
else
   echo "No se encontro el archivo ->  $archivo_or " >> $archivo_log
   error_script=1
fi
echo "Fin del Proceso " `date +%Y/%m/%d-%H:%M:%S` "\n">> $archivo_log

find $ruta_local -name "ussd_123.log.*" -mtime +40 -exec rm {} \;
exit $error_script
